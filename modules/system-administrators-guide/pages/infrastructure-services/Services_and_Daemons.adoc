
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Services_and_Daemons]]
= Services and Daemons
indexterm:[services configuration]
Maintaining security on your system is extremely important, and one approach for this task is to manage access to system services carefully. Your system may need to provide open access to particular services (for example, `httpd` if you are running a web server). However, if you do not need to provide a service, you should turn it off to minimize your exposure to possible bug exploits.

This chapter covers the configuration of the services to be run when a system is started, and provides information on how to start, stop, and restart the services on the command line using the [application]*systemctl* utility.

.Keep the system secure
[IMPORTANT]
====

When you allow access for new services, always remember that both the firewall and [application]*SELinux* need to be configured as well. One of the most common mistakes committed when configuring a new service is neglecting to implement the necessary firewall configuration and SELinux policies to allow access for it.

====

[[s1-services-configuring]]
== Configuring Services
indexterm:[systemctl,services configuration]indexterm:[services configuration,systemctl]
To allow you to configure which services are started at boot time, {MAJOROS} is shipped with the [application]*systemctl* command line tool.

.Do not use the ntsysv and chkconfig utilities
[NOTE]
====

Although it is still possible to use the [application]*ntsysv* and [application]*chkconfig* utilities to manage services that have init scripts installed in the `/etc/rc.d/init.d/` directory, it is advised that you use the [application]*systemctl* utility.

====

.Enabling the irqbalance service
[IMPORTANT]
====

To ensure optimal performance on POWER architecture, it is recommended that the `irqbalance` service is enabled. In most cases, this service is installed and configured to run during the {MAJOROSVER} installation. To verify that `irqbalance` is running, type the following at a shell prompt:

[subs="quotes, macros"]
----
[command]#systemctl status irqbalance.service#
----

====

[[s3-services-configuration-enabling]]
=== Enabling the Service

To configure a service to be automatically started at boot time, use the [command]#systemctl# command in the following form:

----
systemctl enable service_name.service
----

The service will be started the next time you boot the system. For information on how to start the service immediately, refer to xref:Services_and_Daemons.adoc#s3-services-running-running[Running the Service].

[[exam-services-configuration-enabling]]
.Enabling the httpd service
====

Imagine you want to run the Apache HTTP Server on your system. Provided that you have the [package]*httpd* package installed, you can enable the `httpd` service by typing the following at a shell prompt as `root`:

----
~]# systemctl enable httpd.service
----

====

[[s3-services-configuration-disabling]]
=== Disabling the Service

To disable starting a service at boot time, use the [command]#systemctl# command in the following form:

----
systemctl disable service_name.service
----

The next time you boot the system, the service will *not* be started. For information on how to stop the service immediately, refer to xref:Services_and_Daemons.adoc#s3-services-running-stopping[Stopping the Service].

[[exam-services-configuration-disabling]]
.Disabling the telnet service
====

In order to secure the system, users are advised to disable insecure connection protocols such as Telnet. You can make sure that the `telnet` service is disabled by running the following command as `root`:

----
~]# systemctl disable telnet.service
----

====

[[s1-services-running]]
== Running Services
indexterm:[systemctl,services configuration]indexterm:[services configuration,ssystemctl]
The [application]*systemctl* utility also allows you to determine the status of a particular service, as well as to start, stop, or restart a service.

.Do not use the service utility
[NOTE]
====

Although it is still possible to use the [application]*service* utility to manage services that have init scripts installed in the `/etc/rc.d/init.d/` directory, it is advised that you use the [application]*systemctl* utility.

====

[[s3-services-running-checking]]
=== Checking the Service Status

To determine the status of a particular service, use the [command]#systemctl# command in the following form:

----
systemctl status service_name.service
----

This command provides detailed information on the service's status. However, if you merely need to verify that a service is running, you can use the [command]#systemctl# command in the following form instead:

----
systemctl is-active service_name.service
----

[[exam-services-running-checking]]
.Checking the status of the httpd service
====

xref:Services_and_Daemons.adoc#exam-services-configuration-enabling[Enabling the httpd service] illustrated how to enable starting the `httpd` service at boot time. Imagine that the system has been restarted and you need to verify that the service is really running. You can do so by typing the following at a shell prompt:

[subs="quotes, macros"]
----
~]$ [command]#systemctl is-active httpd.service#
active
----

You can also display detailed information about the service by running the following command:

----
~]$ systemctl status httpd.service
httpd.service - LSB: start and stop Apache HTTP Server
          Loaded: loaded (/etc/rc.d/init.d/httpd)
          Active: active (running) since Mon, 23 May 2011 21:38:57 +0200; 27s ago
         Process: 2997 ExecStart=/etc/rc.d/init.d/httpd start (code=exited, status=0/SUCCESS)
        Main PID: 3002 (httpd)
          CGroup: name=systemd:/system/httpd.service
                  ├ 3002 /usr/sbin/httpd
                  ├ 3004 /usr/sbin/httpd
                  ├ 3005 /usr/sbin/httpd
                  ├ 3006 /usr/sbin/httpd
                  ├ 3007 /usr/sbin/httpd
                  ├ 3008 /usr/sbin/httpd
                  ├ 3009 /usr/sbin/httpd
                  ├ 3010 /usr/sbin/httpd
                  └ 3011 /usr/sbin/httpd
----

====

To display a list of all active system services, use the following command:

[subs="quotes, macros"]
----
[command]#systemctl list-units --type=service#
----

This command provides a tabular output with each line consisting of the following columns:

* `UNIT` — A `systemd` unit name. In this case, a service name.

* `LOAD` — Information whether the `systemd` unit was properly loaded.

* `ACTIVE` — A high-level unit activation state.

* `SUB` — A low-level unit activation state.

* `JOB` — A pending job for the unit.

* `DESCRIPTION` — A brief description of the unit.

[[exam-services-running-checking-all]]
.Listing all active services
====

You can list all active services by using the following command:

[subs="quotes, macros"]
----
~]$ [command]#systemctl list-units --type=service#
UNIT                      LOAD   ACTIVE SUB     JOB DESCRIPTION
abrt-ccpp.service         loaded active exited      LSB: Installs coredump handler which saves segfault data
abrt-oops.service         loaded active running     LSB: Watches system log for oops messages, creates ABRT dump directories for each oops
abrtd.service             loaded active running     ABRT Automated Bug Reporting Tool
accounts-daemon.service   loaded active running     Accounts Service
atd.service               loaded active running     Job spooling tools
_[output truncated]_
----

In the example above, the `abrtd` service is loaded, active, and running, and it does not have any pending jobs.

====

[[s3-services-running-running]]
=== Running the Service

To run a service, use the [command]#systemctl# command in the following form:

----
systemctl start service_name.service
----

This will start the service in the current session. To configure the service to be started at boot time, refer to xref:Services_and_Daemons.adoc#s3-services-configuration-enabling[Enabling the Service].

[[exam-services-running-running]]
.Running the httpd service
====

xref:Services_and_Daemons.adoc#exam-services-configuration-enabling[Enabling the httpd service] illustrated how to run the `httpd` service at boot time. You can start the service immediately by typing the following at a shell prompt as `root`:

----
~]# systemctl start httpd.service
----

====

[[s3-services-running-stopping]]
=== Stopping the Service

To stop a service, use the [command]#systemctl# command in the following form:

----
systemctl stop service_name.service
----

This will stop the service in the current session. To disable starting the service at boot time, refer to xref:Services_and_Daemons.adoc#s3-services-configuration-enabling[Enabling the Service].

[[exam-services-running-stopping]]
.Stopping the telnet service
====

xref:Services_and_Daemons.adoc#exam-services-configuration-disabling[Disabling the telnet service] illustrated how to disable starting the `telnet` service at boot time. You can stop the service immediately by running the following command as `root`:

----
~]# systemctl stop telnet.service
----

====

[[s3-services-running-restarting]]
=== Restarting the Service

To restart a service, use the [command]#systemctl# command in the following form:

----
systemctl restart service_name.service
----

[[exam-services-running-restarting]]
.Restarting the sshd service
====

For any changes in the `/etc/ssh/sshd_config` configuration file to take effect, it is required that you restart the `sshd` service. You can do so by typing the following at a shell prompt as `root`:

----
~]# systemctl restart sshd.service
----

====

[[s1-services-additional-resources]]
== Additional Resources

[[s2-services-additional-resources-installed]]
=== Installed Documentation

* `systemctl`(1) — The manual page for the [application]*systemctl* utility.
