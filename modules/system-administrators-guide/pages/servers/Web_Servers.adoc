
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Web_Servers]]
= Web Servers
// indexterm:[HTTP server,Apache HTTP Server]indexterm:[web server,Apache HTTP Server]

[IMPORTANT]
====
**Editor's Note** 

_This guide is deprecated!_  Large parts of it are no longer current and it will not receive any updates. A series of shorter, topic-specific documentation will gradually replace it.

For additional information about Apache Webserver on Fedora see https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-apache-http-server/[Getting started with Apache HTTP Server]
====


A _web server_ is a network service that serves content to a client over the web. This typically means web pages, but any other documents can be served as well. Web servers are also known as HTTP servers, as they use the _hypertext transport protocol_ (*HTTP*).

include::{partialsdir}/servers/The_Apache_HTTP_Server.adoc[]
