:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-System_Locale_and_Keyboard_Configuration]]
= System Locale and Keyboard Configuration

The *system locale* specifies the language settings of system services and user interfaces. The *keyboard layout* settings control the layout used on the text console and graphical user interfaces.

These settings can be made by modifying the `/etc/locale.conf` configuration file or by using the [application]*localectl* utility. You can also set these settings during system installation using the installer graphical interface, text mode interface, or the [command]*keyboard* and [command]*lang* Kickstart commands. See the link:https://docs.fedoraproject.org/en-US/fedora/f{MAJOROSVER}/install-guide[{MAJOROS} Installation Guide] for information about these options.

[[s1-Setting_the_System_Locale]]
== Setting the System Locale

System-wide locale settings are stored in the `/etc/locale.conf` file, which is read at early boot by the `systemd` daemon. The locale settings configured in `/etc/locale.conf` are inherited by every service or user, unless individual programs or individual users override them.

The basic file format of `/etc/locale.conf` is a newline-separated list of variable assignments. For example, German locale with English messages in `/etc/locale.conf` looks as follows:

----

LANG=de_DE.UTF-8
LC_MESSAGES=C

----

Here, the LC_MESSAGES option determines the locale used for diagnostic messages written to the standard error output. To further specify locale settings in `/etc/locale.conf`, you can use several other options, the most relevant are summarized in xref:System_Locale_and_Keyboard_Configuration.adoc#tab-locale_options[Options configurable in /etc/locale.conf]. See the `locale(7)` manual page for detailed information on these options. Note that the LC_ALL option, which represents all possible options, should not be configured in `/etc/locale.conf`.

[[tab-locale_options]]
.Options configurable in /etc/locale.conf

[options="header"]
|===
|Option|Description
|LANG|Provides a default value for the system locale.
|LC_COLLATE|Changes the behavior of functions which compare strings in the local alphabet.
|LC_CTYPE|Changes the behavior of the character handling and classification functions and the multibyte character functions.
|LC_NUMERIC|Describes the way numbers are usually printed, with details such as decimal point versus decimal comma.
|LC_TIME|Changes the display of the current time, 24-hour versus 12-hour clock.
|LC_MESSAGES|Determines the locale used for diagnostic messages written to the standard error output.
|===

[[s2-Displaying_the_Current_Status]]
=== Displaying the Current Status

The [command]#localectl# command can be used to query and change the system locale and keyboard layout settings. To show the current settings, use the [option]`status` option:

[subs="quotes, macros"]
----
[command]#localectl# [option]`status`
----

.Displaying the Current Status
====

The output of the previous command lists the currently set locale, keyboard layout configured for the console and for the X11 window system.

[subs="attributes"]
----

~]${nbsp}localectl status
   System Locale: LANG=en_US.UTF-8
       VC Keymap: us
      X11 Layout: n/a

----

====

[[s2-Listing_Available_Locales]]
=== Listing Available Locales

To list all locales available for your system, type:

[subs="quotes, macros"]
----
[command]#localectl# [option]`list-locales`
----

.Listing Locales
====

Imagine you want to select a specific English locale, but you are not sure if it is available on the system. You can check that by listing all English locales with the following command:

[subs="macros, attributes"]
----

~]${nbsp}localectl list-locales | grep pass:quotes[`en_`]
en_AG
en_AG.utf8
en_AU
en_AU.iso88591
en_AU.utf8
en_BW
en_BW.iso88591
en_BW.utf8

pass:quotes[*output truncated*]

----

====

[[s2-Setting_the_Locale]]
=== Setting the Locale

To set the default system locale, use the following command as `root`:

[subs="quotes, macros"]
----
[command]#localectl# [option]`set-locale` [option]`LANG`pass:attributes[{blank}]=pass:attributes[{blank}]_locale_
----

Replace _locale_ with the locale name, found with the [command]#localectl# [option]`list-locales` command. The above syntax can also be used to configure parameters from xref:System_Locale_and_Keyboard_Configuration.adoc#tab-locale_options[Options configurable in /etc/locale.conf].

.Changing the Default Locale
====

For example, if you want to set British English as your default locale, first find the name of this locale by using [option]`list-locales`. Then, as `root`, type the command in the following form:

[subs="macros, attributes"]
----
~]#{nbsp}localectl set-locale LANG=pass:quotes[`en_GB.utf8`]
----

====

[[s1-Changing_the_Keyboard_Layout]]
== Changing the Keyboard Layout
indexterm:[localectl,keyboard configuration]indexterm:[keyboard configuration,layout]
The keyboard layout settings enable the user to control the layout used on the text console and graphical user interfaces.

[[s2-Displaying_the_Current_Settings]]
=== Displaying the Current Settings

As mentioned before, you can check your current keyboard layout configuration with the following command:

[subs="quotes, macros"]
----
[command]#localectl# [option]`status`
----

.Displaying the Keyboard Settings
====

In the following output, you can see the keyboard layout configured for the virtual console and for the X11 window system.

[subs="attributes"]
----

~]${nbsp}localectl status
   System Locale: LANG=en_US.utf8
       VC Keymap: us
      X11 Layout: us

----

====

[[s2-Listing_Available_Keymaps]]
=== Listing Available Keymaps

To list all available keyboard layouts that can be configured on your system, type:

[subs="quotes, macros"]
----
[command]#localectl# [option]`list-keymaps`
----

.Searching for a Particular Keymap
====

You can use [command]#grep# to search the output of the previous command for a specific keymap name. There are often multiple keymaps compatible with your currently set locale. For example, to find available Czech keyboard layouts, type:

[subs="quotes, macros, attributes"]
----

~]${nbsp}pass:attributes[{blank}][command]#localectl# [option]`list-keymaps` | [command]#grep# `cz`
cz
cz-cp1250
cz-lat2
cz-lat2-prog
cz-qwerty
cz-us-qwertz
sunt5-cz-us
sunt5-us-cz


----

====

[[s2-Setting_the_Keymap]]
=== Setting the Keymap

To set the default keyboard layout for your system, use the following command as `root`:

[subs="quotes, macros"]
----
[command]#localectl# [option]`set-keymap` _map_
----

Replace _map_ with the name of the keymap taken from the output of the [command]#localectl# [option]`list-keymaps` command. Unless the [option]`--no-convert` option is passed, the selected setting is also applied to the default keyboard mapping of the X11 window system, after converting it to the closest matching X11 keyboard mapping. This also applies in reverse, you can specify both keymaps with the following command as `root`:

[subs="quotes, macros"]
----
[command]#localectl# [option]`set-x11-keymap` _map_
----

If you want your X11 layout to differ from the console layout, use the [option]`--no-convert` option.

[subs="quotes, macros"]
----
[command]#localectl# [option]`--no-convert` [option]`set-x11-keymap` _map_
----

With this option, the X11 keymap is specified without changing the previous console layout setting.

.Setting the X11 Keymap Separately
====

Imagine you want to use German keyboard layout in the graphical interface, but for console operations you want to retain the US keymap. To do so, type as `root`:

[subs="macros, attributes"]
----
~]#{nbsp}localectl --no-convert set-x11-keymap pass:quotes[_de_]
----

Then you can verify if your setting was successful by checking the current status:

[subs="attributes"]
----

~]${nbsp}localectl status
   System Locale: LANG=de_DE.UTF-8
       VC Keymap: us
      X11 Layout: de

----

====

Apart from keyboard layout (_map_), three other options can be specified:

[subs="quotes, macros"]
----
[command]#localectl# [option]`set-x11-keymap` _map_ _model_ _variant_ _options_
----

Replace _model_ with the keyboard model name,
_variant_ and _options_ with keyboard variant and option components, which can be used to enhance the keyboard behavior. These options are not set by default. For more information on X11 Model, X11 Variant, and X11 Options see the `kbd(4)` man page.

[[s2-If_Using_Gnome]]
=== Consider this option if using gnome

This should work if the following conditions apply:

* Using gnome as the desktop environment

* Your layout is not listed under Settings -> Keyboard -> Input Sources 

In your terminal type in:

`gsettings set org.gnome.desktop.input-sources show-all-sources true`

after pressing return, try looking under your keyboard settings again as there should be a lot more options available.
As the proper source for help is gnome in this case, here is more information from gnome:

https://help.gnome.org/users/gnome-help/stable/keyboard-layouts.html.en

[[sect-Keyboard_Configuration-Resources]]
== Additional Resources

For more information on how to configure the keyboard layout on Fedora, see the resources listed below:

.Installed Documentation

* `localectl`(1) — The manual page for the [command]#localectl# command line utility documents how to use this tool to configure the system locale and keyboard layout.

* `loadkeys`(1) — The manual page for the [command]#loadkeys# command provides more information on how to use this tool to change the keyboard layout in a virtual console.
