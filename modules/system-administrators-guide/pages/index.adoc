:experimental:
include::{partialsdir}/entities.adoc[]

= System Administrator's Guide

Deployment, Configuration, and Administration of {MAJOROSVER}

[IMPORTANT]
====
**Editor's Note** 

_This guide is deprecated!_ Large parts of it are no longer current and it will not receive any updates. A series of shorter, topic-specific documentation will gradually replace it.
====

[abstract]
--

The [citetitle]_System Administrator's Guide_ documents relevant information regarding the deployment, configuration, and administration of {MAJOROSVER}. It is oriented towards system administrators with a basic understanding of the system.

--
image:title_logo.svg[Fedora Documentation Team]
include::{partialsdir}/Legal_Notice.adoc[]

include::{partialsdir}/Author_Group.adoc[]
